FROM nginx:latest

COPY docker-entrypoint.d/* /docker-entrypoint.d/
RUN chmod a+x /docker-entrypoint.d/set_config.sh
