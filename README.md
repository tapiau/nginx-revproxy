# Nginx Revproxy

Simple nginx-based http revproxy.

Just set PROXY_TARGET to the target URL and run the container.

```docker run -it --env PROXY_TARGET=http://192.168.1.123:8123 np1```
