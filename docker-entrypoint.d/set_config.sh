#!/usr/bin/env sh

if [ -z ${PROXY_TARGET} ]; then
    echo "PROXY_TARGET is not set"
    exit 1
fi

echo "
server {
    listen *:80;
    resolver 127.0.0.11 valid=30s;
    index index.html index.htm;
    location / {
        proxy_pass ${PROXY_TARGET};
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_buffering off;
        proxy_redirect off;

        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \"upgrade\";
    }
}
" > /etc/nginx/conf.d/default.conf
